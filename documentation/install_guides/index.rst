.. _install_guides_index:

Installation and Getting Started
################################

This section guides you through the necessary operating system preparation and
the installation and configuration process of the |oA| software.

The installation can be broken down into the following steps:

#) :ref:`base operating system installation`

#) openATTIC installation:
    * :ref:`installation on opensuse leap`

#) :ref:`post-installation configuration`

#) :ref:`getting started`

.. toctree::
  :maxdepth: 2

  oA_installation
  post_installation
  getting_started
  authentication
  config_recommendations
  troubleshooting
