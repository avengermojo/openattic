.. _getting started:

Getting started
===============

Accessing the Web UI
--------------------

After |oA|  has been installed, it can be managed using the web-based user
interface.

Open a web browser and navigate to http://openattic.yourdomain.com/openattic

The default login username is **openattic**. Use the password you defined during
the :ref:`post-installation configuration`.

See the :ref:`gui_docs_index` for further information.
